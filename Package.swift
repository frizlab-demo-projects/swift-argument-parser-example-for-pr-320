// swift-tools-version:5.4
import PackageDescription


let package = Package(
	name: "swift-argument-parser-example-for-pr-320",
	products: [
		.executable(name: "example", targets: ["example"])
	],
	dependencies: [
		.package(url: "https://github.com/apple/swift-argument-parser.git", from: "0.4.3")
	],
	targets: [
		.executableTarget(name: "example", dependencies: [
			.product(name: "ArgumentParser", package: "swift-argument-parser"),
		])
	]
)
