import Foundation

import ArgumentParser



struct Example : ParsableCommand {
	
	@Option
	var option1: String?
	
	@Argument(completion: .custom(completionForArgument))
	var arg: String
	
	func run() throws {
		print(#"HA! This program does nothing \o/ Try autocompletion though :)"#)
	}
	
	private static func completionForArgument(_ args: [String]) -> [String] {
		let debugOutputPath = "./completion_debug.txt"
		if !FileManager.default.fileExists(atPath: debugOutputPath) {
			FileManager.default.createFile(atPath: debugOutputPath, contents: nil)
		}
		let debugOutputFileHandle = try! FileHandle(forWritingTo: URL(fileURLWithPath: debugOutputPath))
		defer {try! debugOutputFileHandle.close()}
		debugOutputFileHandle.seekToEndOfFile()
		debugOutputFileHandle.write(Data("args: \(args)\n".utf8))
		
		return ["nothing to see here", "go away"]
	}
	
}

Example.main()
